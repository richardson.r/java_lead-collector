package br.com.lead.collector.controllers;

import br.com.lead.collector.DTOs.CadastroDeLeadDTO;
import br.com.lead.collector.DTOs.IdProdutoDTO;
import br.com.lead.collector.DTOs.ResumoDeLeadDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import br.com.lead.collector.repositories.ProdutoRepository;
import br.com.lead.collector.services.LeadService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;

@WebMvcTest(LeadController.class) //Permite a simulação de requisições web falsas
public class LeadControllerTeste {

    @MockBean
    private LeadService leadService;

    @Autowired
    private MockMvc mockMvc;

    Lead lead;
    Produto produto;
    List<Produto> produtos;
    List<ResumoDeLeadDTO> resumoDeLeadDTOs;
    CadastroDeLeadDTO cadastroDeLeadDTO;

    @BeforeEach
    public void setup() {
        this.lead = new Lead();
        lead.setId(1);
        lead.setNome("Teste");
        lead.setCpf("33357755890");
        lead.setEmail("teste@email.com");
        lead.setTelefone("(11) 987654-1234");
        lead.setDataDeCadastro(LocalDate.now());

        this.produto = new Produto();
        produto.setId(1);
        produto.setPreco(10.00);
        produto.setNome("Teste");
        produto.setDescricao("Produto teste");

        this.produtos = Arrays.asList(produto);
        lead.setProdutos(produtos);

        resumoDeLeadDTOs = new ArrayList<ResumoDeLeadDTO>();

        this.cadastroDeLeadDTO = new CadastroDeLeadDTO();
        cadastroDeLeadDTO.setTelefone("987654321");
        cadastroDeLeadDTO.setNome("Teste cadastro lead DTO");
        cadastroDeLeadDTO.setCpf("33357755890");
        cadastroDeLeadDTO.setEmail("teste@email.com");

        List<IdProdutoDTO> iDprodutoDTOs = new ArrayList<IdProdutoDTO>();
        cadastroDeLeadDTO.setProdutos(iDprodutoDTOs);
    }

    @Test
    public void testarBuscarPorId() throws Exception {
        Mockito.when(leadService.buscarLeadPeloId(1)).thenReturn(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarBuscarPorIdQueNaoExiste() throws Exception {
        Mockito.when(leadService.buscarLeadPeloId(Mockito.anyInt())).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/" + lead.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarLerTodosOsLeads() throws Exception {
        ResumoDeLeadDTO resumoDeLeadDTO = new ResumoDeLeadDTO();
        resumoDeLeadDTO.setId(1);
        resumoDeLeadDTO.setNome("Teste");
        resumoDeLeadDTO.setEmail("teste@email.com");
        resumoDeLeadDTOs.add(resumoDeLeadDTO);

        Mockito.when(leadService.lerTodosOsLeads()).thenReturn(resumoDeLeadDTOs);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$").isArray());
    }

    @Test
    public void testarCadastrarLead() throws Exception {
        Mockito.when(leadService.salvarLead(Mockito.any(CadastroDeLeadDTO.class))).thenReturn(lead);

        ObjectMapper objectMapper = new ObjectMapper(); //Serve para converter json em objetos java e vice-versa

        String leadJson = objectMapper.writeValueAsString(cadastroDeLeadDTO);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON).content(leadJson))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Teste")));
    }
}
