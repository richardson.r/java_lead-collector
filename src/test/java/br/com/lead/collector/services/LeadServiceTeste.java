package br.com.lead.collector.services;

import br.com.lead.collector.DTOs.CadastroDeLeadDTO;
import br.com.lead.collector.DTOs.IdProdutoDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class LeadServiceTeste {

    @MockBean //Objeto manipulado (mockado)
    private LeadRepository leadRepository;

    @MockBean //Objeto manipulado (mockado)
    private ProdutoRepository produtoRepository;

    @Autowired //Objeto verdadeiro
    private LeadService leadService;

    Lead lead;
    Produto produto;
    CadastroDeLeadDTO cadastroDeLeadDTO;

    @BeforeEach
    public void setup() {
        this.lead = new Lead();
        lead.setId(1);
        lead.setNome("Teste");
        lead.setCpf("33357755890");
        lead.setEmail("teste@email.com");
        lead.setTelefone("(11) 987654-1234");
        lead.setDataDeCadastro(LocalDate.now());

        this.produto = new Produto();
        produto.setId(1);
        produto.setPreco(10.00);
        produto.setNome("Teste");
        produto.setDescricao("Produto teste");

        List<Produto> produtos = Arrays.asList(produto);
        lead.setProdutos(produtos);

        cadastroDeLeadDTO = new CadastroDeLeadDTO();
        cadastroDeLeadDTO.setNome("TesteDTO");
        cadastroDeLeadDTO.setCpf("33357755890");
        cadastroDeLeadDTO.setEmail("teste@email.com");
        cadastroDeLeadDTO.setTelefone("(11) 987654-1234");

        IdProdutoDTO idProdutoDTO = new IdProdutoDTO();
        idProdutoDTO.setId(1);
        cadastroDeLeadDTO.setProdutos(Arrays.asList(idProdutoDTO));
    }

    @Test
    public void testarBuscaDeLeadPeloId() {
        Optional<Lead> leadOptional = Optional.of(lead);
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);

        Lead leadObjeto = leadService.buscarLeadPeloId(1);

        Assertions.assertEquals(lead, leadService.buscarLeadPeloId(1));
        Assertions.assertEquals(lead.getDataDeCadastro(), leadObjeto.getDataDeCadastro());
    }

    @Test
    public void testarBuscaDeLeadPeloIdQueNaoExiste() {
        Optional<Lead> leadOptional = Optional.empty();
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(leadOptional);

        Assertions.assertThrows(RuntimeException.class, () -> {leadService.buscarLeadPeloId(1);});
    }

    @Test
    public void testarSalvarLead(){
        lead.setDataDeCadastro(null);
        Mockito.when(produtoRepository.findAllById(Mockito.any())).thenReturn(lead.getProdutos());
        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).then(objeto -> objeto.getArgument(0)); // pega o argumento da chamada do método

        Lead resultado = leadService.salvarLead(cadastroDeLeadDTO);

        Assertions.assertEquals("TesteDTO", resultado.getNome());
        Assertions.assertNotEquals(lead.getDataDeCadastro(), resultado.getDataDeCadastro());
    }
}
