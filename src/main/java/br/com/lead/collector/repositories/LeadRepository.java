package br.com.lead.collector.repositories;

import br.com.lead.collector.models.Lead;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

// Extende a tabela e o tipo da chave primária
public interface LeadRepository extends CrudRepository<Lead, Integer> {

    Lead findFirstByCpf(String cpf);


    // Abaixo 2 formas de fazer a pesquisa por email e nome.
    //
    //Reservado
    List<Lead> findByEmailAndNome(String email, String nome);
    //
    //Query específica
    @Query(value = "SELECT * FROM lead WHERE email = :email AND nome = :nome")
    Lead buscarPorEmailAndNome(String email, String nome);
}
