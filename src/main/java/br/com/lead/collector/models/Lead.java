package br.com.lead.collector.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.time.LocalDate;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

// Usa o @Entity para o que o Hibernate identifique essa classe como uma tabela
@Entity
//@Table(name = "leads") //Associa tabela caso a tabela já exista e tenha nome diferente
public class Lead {

    @Id
    @GeneratedValue(strategy = IDENTITY) //Gera id da coluna primária
    private int id;

    //Neste caso as validações serão feitas na classe DTO
    private String nome;
    private String cpf;
    private String email;
    private String telefone;
    private LocalDate dataDeCadastro;

    @ManyToMany //Gera a tabela de relacionamento com a Produtos
    private List<Produto> produtos;


    public Lead() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }

    public LocalDate getDataDeCadastro() {
        return dataDeCadastro;
    }

    public void setDataDeCadastro(LocalDate dataDeCadastro) {
        this.dataDeCadastro = dataDeCadastro;
    }
}
