package br.com.lead.collector.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.*;

@Entity
public class Produto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //Gera id da coluna primária
    private int id;

    //@Column(name = "nome_cliente") //Associa coluna caso já exista com outro nome
    @NotNull(message = "Nome não pode ser nullo")
    @NotBlank(message = "Nome não pode estar em branco")
    @Size(min = 3, message = "Nome no mínimo com 3 caracteres")
    private String nome;

    @NotBlank(message = "A descrição não pode estar em branco")
    @NotNull(message = "A descrição não pode ser null")
    private String descricao;

    @NotNull(message = "O preço não pode ser null")
    @Digits(integer = 6, fraction = 2, message = "Preço fora do padrão")
    @DecimalMin(value = "0.10",message = "O preço não pode ser inferior a 0.10")
    private Double preco;

    public Produto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getPreco() {
        return preco;
    }

    public void setPreco(Double preco) {
        this.preco = preco;
    }
}
