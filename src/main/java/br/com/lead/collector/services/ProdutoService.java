package br.com.lead.collector.services;

import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProdutoService {

    @Autowired
    private ProdutoRepository produtoRepository;

    public Produto salvarProduto(Produto produto) {
        return produtoRepository.save(produto);
    }

    public Iterable<Produto> lerTodosOsProdutos() {
        return produtoRepository.findAll();
    }

    public Produto buscarProdutoPeloId(int id) {
        Optional<Produto> produtoOptional = produtoRepository.findById(id);
        if (produtoOptional.isPresent())
            return produtoOptional.get();
        else {
            throw new RuntimeException("Produto(" + id + ") não encontrado");
        }
    }

    //Atualizar com PUT
    public Produto atualizarProduto(int id, Produto produto) {
        Produto produtoDB = buscarProdutoPeloId(id);
        produto.setId(produtoDB.getId());
        return produtoRepository.save(produto);
    }

    public void deletarProduto(int id) {
        if (produtoRepository.existsById(id))
            produtoRepository.deleteById(id);
        else
            throw new RuntimeException("Registro não existe");
    }
}
