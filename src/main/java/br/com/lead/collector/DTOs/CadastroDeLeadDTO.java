package br.com.lead.collector.DTOs;

import br.com.lead.collector.models.Lead;

import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.Column;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

public class CadastroDeLeadDTO {
    //@Column(name = "nome_cliente") //Associa coluna caso já exista com outro nome
    @NotNull(message = "Nome não pode ser nullo")
    @NotBlank(message = "Nome não pode estar em branco")
    @Size(min = 3, message = "Nome no mínimo com 3 caracteres")
    private String nome;

    @CPF(message = "CPF deve estar em um formato válido")
    @NotNull
    @Column(unique = true)
    private String cpf;

    @NotBlank(message = "E-mail não pode estar em branco")
    @Email(message = "E-mail inválido")
    @NotNull
    private String email;

    private String telefone;

    @NotNull
    private List<IdProdutoDTO> produtos;

    public CadastroDeLeadDTO() {
    }


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public List<IdProdutoDTO> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<IdProdutoDTO> produtos) {
        this.produtos = produtos;
    }

    public Lead converterParaLead(){
        Lead lead = new Lead();
        lead.setNome(this.nome);
        lead.setCpf(this.cpf);
        lead.setEmail(this.email);
        lead.setTelefone(this.telefone);

        return lead;
    }
}
