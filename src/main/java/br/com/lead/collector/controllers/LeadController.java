package br.com.lead.collector.controllers;

import br.com.lead.collector.DTOs.CadastroDeLeadDTO;
import br.com.lead.collector.DTOs.ResumoDeLeadDTO;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.services.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/leads")
public class LeadController {

    @Autowired
    private LeadService leadService;

    @GetMapping //get leads
    public List<ResumoDeLeadDTO> lerTodosOsLeads() {
        return leadService.lerTodosOsLeads();
    }

    @PostMapping //POST da página leads
    @ResponseStatus(HttpStatus.CREATED) // 201 Created
    public Lead cadastrarLead(@RequestBody @Valid CadastroDeLeadDTO cadastroDeLeadDTO) {
        return leadService.salvarLead(cadastroDeLeadDTO);
    }

    @GetMapping("/{id}")
    public Lead pesquisarPorId(@PathVariable(name = "id") int id) {
        try {
            Lead lead = leadService.buscarLeadPeloId(id);
            return lead;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Lead atualizarLead(@RequestBody @Valid CadastroDeLeadDTO cadastroDeLeadDTO, @PathVariable(name = "id") int id) {
        try {
            return leadService.atualizarLead(id, cadastroDeLeadDTO);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    // (204 No Content - Requisição atendida mas o corpo está vazio (não precisa atualizar a página)
    public void deletarLead(@PathVariable(name = "id") int id) {
        try {
            leadService.deletarLead(id);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/detalhes")
    public Lead detalhesDeLead(@RequestParam(name = "cpf") String cpf) {
        try {
            return leadService.pesquisarPorCPF(cpf);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
